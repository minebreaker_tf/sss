package sss.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public final class WebController {

    @GetMapping("/")
    public String getIndex(Model model) {
        return "index";
    }

}

package sss.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

@Service
public final class ClassificationService {

    private static final Logger logger = LoggerFactory.getLogger(ClassificationService.class);

    private final Classifier classifier;
    private final Path tempDir;

    @Autowired
    public ClassificationService(
            Classifier classifier,
            FileSystemConfiguration configuration) {
        this.classifier = classifier;
        String tempDir = configuration.getTempDir();
        FileSystem fileSystem = configuration.getFileSystem();
        this.tempDir = fileSystem.getPath(tempDir);
    }

    public FileUploadResult classify(MultipartFile file) throws IOException {
        Path picture = save(file);

        FileUploadResult result = classifier.classify(picture);
        result.setOriginalFilename(file.getOriginalFilename());

        Files.delete(picture);
        Files.delete(picture.getParent());
        return result;
    }

    private Path save(MultipartFile file) throws IOException {
        if (file.isEmpty()) throw new FileNotFoundException(file.getOriginalFilename());
        UUID name = UUID.randomUUID();
        Path dest = tempDir.resolve(name.toString()).resolve(file.getOriginalFilename());
        logger.debug("Dest: {}", dest);
        Files.createDirectory(dest.getParent());
        Files.copy(file.getInputStream(), dest);
        return dest;
    }

}

package sss.ui;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;

/**
 * Must not be final.
 */
@NoArgsConstructor
@ToString
@Configuration
public class FileSystemConfiguration {

    @Getter
    @Setter
    private String tempDir = System.getProperty("java.io.tmpdir");

    @Getter
    @Setter
    private FileSystem fileSystem = FileSystems.getDefault();

    @Getter
    @Setter
    @Value("${network}")
    private String modelName;

}

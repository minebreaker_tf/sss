package sss.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public final class FileUploadController {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

    private final ClassificationService manager;

    @Autowired
    public FileUploadController(ClassificationService manager) {
        this.manager = manager;
    }

    @PostMapping("/upload")
    public FileUploadResult handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
        logger.info("Received file name: {}", file.getOriginalFilename());
        return manager.classify(file);
    }

    @ExceptionHandler(IOException.class)
    public FileUploadResult handleException(IOException e) {
        logger.info("File upload failed.", e);
        return new FileUploadResult("failure", e.getLocalizedMessage(), null, null);
    }

}

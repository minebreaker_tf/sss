package sss.ui;

import lombok.*;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class FileUploadResult {

    @Getter
    @Setter
    @NotNull
    private String status;

    @Getter
    @Setter
    @NotNull
    private String result;

    @Getter
    @Setter
    private String raw;

    @Getter
    @Setter
    private String originalFilename;

    public FileUploadResult(String status, String result, String raw, String originalFilename) {
        this.status = status;
        this.result = result;
        this.raw = raw;
        this.originalFilename = originalFilename;
    }

}

package sss.ui;

import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

@Service
public final class Classifier {

    private static final int height = 100;
    private static final int width = 100;
    private static final int channels = 3;

    private final MultiLayerNetwork network;

    @Autowired
    public Classifier(FileSystemConfiguration configuration) throws IOException {
        Path network = configuration.getFileSystem().getPath(configuration.getModelName());
        this.network = ModelSerializer.restoreMultiLayerNetwork(network.toFile());
    }

    public FileUploadResult classify(Path picture) throws IOException {

        FileSplit testData = new FileSplit(picture.getParent().toFile(), NativeImageLoader.ALLOWED_FORMATS);

        ImageRecordReader recordReader = new ImageRecordReader(height, width, channels);
        recordReader.initialize(testData);
        DataSetIterator dataIter = new RecordReaderDataSetIterator(recordReader, 1);

        DataSet data = dataIter.next();
        data.setLabelNames(Arrays.asList("mizugi", "seihuku", "sukumizu"));
        String raw = network.output(data.getFeatures()).toString();
        String res = network.predict(data).get(0);

        return new FileUploadResult("success", res, raw, null);
    }

}

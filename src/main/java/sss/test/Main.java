package sss.test;

import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.api.split.InputSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.datavec.image.transform.FlipImageTransform;
import org.datavec.image.transform.ImageTransform;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.datasets.iterator.MultipleEpochsIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.LearningRatePolicy;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.distribution.GaussianDistribution;
import org.deeplearning4j.nn.conf.distribution.NormalDistribution;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public final class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static int height = 100;
    private static int width = 100;
    private static int channels = 3;
    private static int numExamples = 2200;
    private static int numLabels = 3;
    private static int batchSize = 400;

    private static long seed = 42;
    private static Random rng = new Random(seed);
    private static int listenerFreq = 1;
    private static int iterations = 1;
    private static int epochs = 50;
    private static int nCores = 2;

    public static void main(String[] args) throws IOException {

//        DataTypeUtil.setDTypeForContext(DataBuffer.Type.HALF);
//        CudaEnvironment.getInstance().getConfiguration()
//                       .setMaximumDeviceCache(2L * 1024L * 1024L * 1024L);

        UIServer server = UIServer.getInstance();
        StatsStorage storage = new InMemoryStatsStorage();
        server.attach(storage);

        Instant start = Instant.now();
        logger.info("Load data...");

        ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
        File mainPath = new File("images/");
        FileSplit fileSplit = new FileSplit(mainPath, NativeImageLoader.ALLOWED_FORMATS, rng);
        BalancedPathFilter pathFilter = new BalancedPathFilter(rng, labelMaker, numExamples, numLabels, batchSize);

        InputSplit[] inputSplit = fileSplit.sample(pathFilter, 90, 10);
        InputSplit trainData = inputSplit[0];
        InputSplit testData = inputSplit[1];

        ImageTransform flipTransformX = new FlipImageTransform(0);
        ImageTransform flipTransformY = new FlipImageTransform(1);
        ImageTransform flipTransformXY = new FlipImageTransform(-1);
        List<ImageTransform> transforms = Arrays.asList(null, flipTransformX, flipTransformY, flipTransformXY);

        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);

        logger.info("Build model....");

        double nonZeroBias = 1;
        double dropOut = 0.5;
        MultiLayerNetwork network = new MultiLayerNetwork(
                new NeuralNetConfiguration.Builder()
                        .seed(seed)
                        .weightInit(WeightInit.DISTRIBUTION)
                        .dist(new NormalDistribution(0.0, 0.01))
                        .activation(Activation.RELU)
                        .updater(Updater.NESTEROVS)
                        .iterations(iterations)
                        .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer)
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                        .learningRate(0.02) // 0.01
                        .biasLearningRate(0.04) // 0.02
                        // 0.008 : 0.008 -> Too small
                        // 0.02 : 0.04 was better than 0.01 : 0.02
                        // 0.04 : 0.04 -> NaN
                        // 0.04 : 0.08 -> NaN
                        .learningRateDecayPolicy(LearningRatePolicy.Step)
                        .lrPolicyDecayRate(0.1)
                        .lrPolicySteps(100000)
                        .regularization(true)
                        .l2(5 * 1e-4)
                        .momentum(0.9)
                        .miniBatch(false)
                        .list()
                        .layer(0, new ConvolutionLayer.Builder(new int[] { 11, 11 }, new int[] { 4, 4 }, new int[] { 3, 3 })
                                .name("cnn1")
                                .nIn(channels)
                                .nOut(96)
                                .biasInit((double) 0)
                                .build())
                        .layer(1, new LocalResponseNormalization.Builder().name("lrn1").build())
                        .layer(2, new SubsamplingLayer.Builder(new int[] { 3, 3 }, new int[] { 2, 2 }).name("maxpool1").build())
                        .layer(3, new ConvolutionLayer.Builder(new int[] { 5, 5 }, new int[] { 1, 1 }, new int[] { 2, 2 })
                                .name("cnn2")
                                .nOut(256)
                                .biasInit(nonZeroBias)
                                .build())
                        .layer(4, new LocalResponseNormalization.Builder().name("lrn2").build())
                        .layer(5, new SubsamplingLayer.Builder(new int[] { 3, 3 }, new int[] { 2, 2 }).name("maxpool2").build())
                        .layer(6, new ConvolutionLayer.Builder(new int[] { 3, 3 }, new int[] { 1, 1 }, new int[] { 1, 1 })
                                .name("cnn3")
                                .nOut(384)
                                .biasInit((double) 0)
                                .build())
                        .layer(7, new ConvolutionLayer.Builder(new int[] { 3, 3 }, new int[] { 1, 1 }, new int[] { 1, 1 })
                                .name("cnn4")
                                .nOut(384)
                                .biasInit(nonZeroBias)
                                .build())
                        .layer(8, new ConvolutionLayer.Builder(new int[] { 3, 3 }, new int[] { 1, 1 }, new int[] { 1, 1 })
                                .name("cnn5").nOut(256)
                                .biasInit(nonZeroBias)
                                .build())
                        .layer(9, new SubsamplingLayer.Builder(new int[] { 3, 3 }, new int[] { 2, 2 }).name("maxpool3").build())
                        .layer(10, new DenseLayer.Builder()
                                .name("ffn1")
                                .nOut(4096)
                                .biasInit(nonZeroBias)
                                .dropOut(dropOut)
                                .dist(new GaussianDistribution(0, 0.005))
                                .build())
                        .layer(11, new DenseLayer.Builder()
                                .name("ffn2")
                                .nOut(4096)
                                .biasInit(nonZeroBias)
                                .dropOut(dropOut)
                                .dist(new GaussianDistribution(0, 0.005))
                                .build())
                        .layer(12, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                                .name("output")
                                .nOut(numLabels)
                                .activation(Activation.SOFTMAX)
                                .build())
                        .backprop(true)
                        .pretrain(false)
                        .setInputType(InputType.convolutional(height, width, channels))
                        .build());
        network.init();
        network.setListeners(new ScoreIterationListener(listenerFreq), new StatsListener(storage));

        ImageRecordReader recordReader = new ImageRecordReader(height, width, channels, labelMaker);
        DataSetIterator dataIter;
        MultipleEpochsIterator trainIter;

        logger.info("Train model....");
        for (ImageTransform transform : transforms) {
            logger.info("\n{}\n", transform);
            recordReader.initialize(trainData, transform);
            dataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, numLabels);
            scaler.fit(dataIter);
            dataIter.setPreProcessor(scaler);
            trainIter = new MultipleEpochsIterator(epochs, dataIter, nCores);
            network.fit(trainIter);
        }

        logger.info("Evaluate model....");
        recordReader.initialize(testData);
        dataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, numLabels);
        scaler.fit(dataIter);
        dataIter.setPreProcessor(scaler);
        Evaluation eval = network.evaluate(dataIter);
        logger.info("{}", eval.stats(false));

        dataIter.reset();
        logger.info("{}", network.output(dataIter.next().getFeatures()));

        Instant end = Instant.now();

        File saveTo = new File("tempModel.zip");
        ModelSerializer.writeModel(network, saveTo, false);

        logger.info("Start: {}", start);
        logger.info("End:   {}", end);
    }

}

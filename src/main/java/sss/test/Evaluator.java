package sss.test;

import org.datavec.api.io.labels.PathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.api.writable.Text;
import org.datavec.api.writable.Writable;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Random;

public class Evaluator {

    private static final Logger logger = LoggerFactory.getLogger(Evaluator.class);

    private static int height = 100;
    private static int width = 100;
    private static int channels = 3;
    private static int numLabels = 3;
    private static int batchSize = 300;

    private static long seed = 42;
    private static Random rng = new Random(seed);

    public static void main(String[] args) throws IOException {

//        DataTypeUtil.setDTypeForContext(DataBuffer.Type.HALF);
//        CudaEnvironment.getInstance().getConfiguration()
//                       .setMaximumDeviceCache(2L * 1024L * 1024L * 1024L);

        File saveTo = new File("tempModel.zip");
        MultiLayerNetwork network = ModelSerializer.restoreMultiLayerNetwork(saveTo, true);

        File mainPath = new File("predict");
        FileSplit testData = new FileSplit(mainPath, NativeImageLoader.ALLOWED_FORMATS, rng);

        PathLabelGenerator labelGenerator = new PathLabelGenerator() {

            @Override
            public Writable getLabelForPath(String path) {
                if (path.contains("mizugi"))
                    return new Text("mizugi");
                else if (path.contains("seihuku"))
                    return new Text("seihuku");
                else if (path.contains("sukumizu"))
                    return new Text("sukumizu");
                else
                    throw new IllegalArgumentException(path);
            }

            @Override
            public Writable getLabelForPath(URI uri) {
                return getLabelForPath(new File(uri).toString());
            }
        };

        ImageRecordReader recordReader = new ImageRecordReader(height, width, channels, labelGenerator);
        recordReader.initialize(testData);

        DataSetIterator dataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, numLabels);
        for (DataSet ds : dataIter.next()) {
            ds.setLabelNames(recordReader.getLabels());
            logger.info("Predict: {}", network.predict(ds));
            logger.info("Output:  {}", network.output(ds.getFeatures()));
        }

        dataIter.reset();
        Evaluation eval = network.evaluate(dataIter);
        logger.info("\n{}", eval.confusionToString());
        logger.info(eval.stats());

    }

}

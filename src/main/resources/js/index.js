(function ($, window, document) {
    'use strict';

    $(function () {
        setDndHandler();
        setFileChooserHandler();
    });

    var setDndHandler = function () {
        var dropbox = $('#dropbox');
        var cancelEvent = function (e) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        };
        dropbox.on('drop', function (e) {
            cancelEvent(e);

            var file = e.originalEvent.dataTransfer.files[0];
            sendFile(file);
        });

        // Remove default handlers
        dropbox.on('dragenter', cancelEvent);
        dropbox.on('dragover', cancelEvent);
        var doc = $(document);
        doc.on('drop', cancelEvent);
        doc.on('dragenter', cancelEvent);
        doc.on('dragover', cancelEvent);
    };

    var setFileChooserHandler = function () {
        var chooser = $('#chooser');
        chooser.on('change', function () {
            var file = chooser.prop('files')[0];
            sendFile(file);
        });
    };

    var sendFile = function (file) {
        showLoader();
        if (!file) {
            return;
        }

        if (file.size > 5 * 1000 * 1000) {
            removeLoader();
            $('#result').text('File size too big: ' + file.size);
            return;
        }

        var response = uploadFile(file);
        response.done(function (data) {
            console.log(data);
            if (data.status === 'success') {
                $('#result').text('Classified as: ' + data.result);
            } else {
                $('#result').text('Sorry, the classification had failed.');
            }
        }).fail(function (jqXHR, status, errorThrown) {
            console.log(errorThrown);
            $('#result').text('Sorry, an unexpected error had occurred: ' + status);
        }).always(function () {
            removeLoader();
        });
    };

    var token = $("meta[name='_csrf']").attr("content");
    var uploadFile = function (file) {
        var form = new FormData();
        form.append('file', file);

        return $.ajax({
            url: '/upload',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            data: form,
            processData: false,
            contentType: false
        });
    };

    var loader = $('#loader');
    var showLoader = function () {
        loader.addClass('loader');
    };
    var removeLoader = function () {
        loader.removeClass('loader');
    };

}(window.jQuery, window, document));

package sss.ui;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ClassifierITest {

    private Classifier classifier;

    @Before
    public void setUp() throws Exception {
        FileSystemConfiguration config = new FileSystemConfiguration();
        config.setModelName("./tempModel.zip");
        this.classifier = new Classifier(config);
    }

    @Ignore
    @Test
    public void test() throws IOException {
        FileUploadResult res = classifier.classify(Paths.get("./predict2/mizugi/1.jpg"));
        System.out.println(res);

        FileUploadResult res2 = classifier.classify(Paths.get("./predict2/seihuku/1.jpg"));
        System.out.println(res2);

        FileUploadResult res3 = classifier.classify(Paths.get("./predict2/sukumizu/1.jpg"));
        System.out.println(res3);

        assertThat(res.getResult(), is("mizugi"));
        assertThat(res2.getResult(), is("seihuku"));
        assertThat(res3.getResult(), is("sukumizu"));
    }

}
